do_install_append () {
	echo "PS1='\h:\w# '" >> ${D}/${sysconfdir}/profile

	# fstab
	cat > ${D}/${sysconfdir}/fstab << EOF
/dev/root            /                    auto       defaults              1  1
proc                 /proc                proc       defaults              0  0
devpts               /dev/pts             devpts     mode=0620,gid=5       0  0
tmpfs                /run                 tmpfs      mode=0755,nodev,nosuid,strictatime 0  0
tmpfs                /var/volatile        tmpfs      defaults              0  0
EOF
	# create /var/volatile/
	mkdir -p ${D}/${localstatedir}/volatile

}
